ReadMe File

ABQ-Data for City of Albuquerque, 1.0 by MyChelle Andrews

May 16, 2018

Contents
I. 	What has changed
	- File Structure has change
	- MetaData
II.	Location 
	- http://data.cabq.gov/publicsafety/policeincidents/
III.	Description of change
	- Added changed fields:
          - CV_BLOCK_ADD is now BlockAddress
          - CVINC_TYPE is now IncidentType
        - Updated files:
          - MetaData.pdf

